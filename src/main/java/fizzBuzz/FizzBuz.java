package fizzBuzz;

public class FizzBuz {

	public static boolean checkFizz(int number) {
		return ( ( number % 3 ) == 0 );
	}

	public static boolean checkBuzz(int number) {
		return ( ( number % 5 ) == 0 );
	}
	
	public static boolean checkFizzBuzz(int number) {
		return ( ( number % 15 ) == 0 );
	}

}
