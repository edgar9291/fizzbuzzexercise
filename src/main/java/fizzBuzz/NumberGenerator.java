package fizzBuzz;

import java.util.ArrayList;
import java.util.List;

public class NumberGenerator {
 
	public static List<Integer> generateNumbers(Integer start , Integer end){
    	List<Integer> numerosGenerados = new ArrayList<>();
    	for( Integer i = start ; i <= end ; i++ ) {
    		numerosGenerados.add(i);
    	}
    	return numerosGenerados;
    }

}
